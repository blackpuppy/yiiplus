<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'params' => require(__DIR__ . '/params-local.php'),
    'timeZone' => 'Asia/Singapore',
];
