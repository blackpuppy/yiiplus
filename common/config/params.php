<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'domains' => [
        'en' => [
            'language' => 'en-US',
            'name' => 'English',
            'domain' => 'en.poc.foreign-study.net',
        ],
        'cn' => [
            'language' => 'zh-CN',
            'name' => 'Simplified Chinese',
            'domain' => 'cn.poc.foreign-study.net',
        ],
        'tw' => [
            'language' => 'zh-TW',
            'name' => 'Traditional Chinese',
            'domain' => 'tw.poc.foreign-study.net',
        ],
        'jp' => [
            'language' => 'ja',
            'name' => 'Japanese',
            'domain' => 'jp.poc.foreign-study.net',
        ],
    ],
];
