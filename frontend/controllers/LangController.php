<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * LangController implements the CRUD actions for Status model.
 */
class LangController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'switch' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['switch'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        // 'roles' => ['?'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Switches to a different language.
     * @return mixed
     */
    public function actionSwitch()
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Only AJAX request is allowed.');
        }

        Yii::info('LangController::actionSwitch():');

        $data = Yii::$app->request->post();
        Yii::info('  $data = '. print_r($data, true));

        $languageCode = $data['language'];
        Yii::info('  $languageCode = '. $languageCode);

        $oldLang = Yii::$app->language;

        $domains = Yii::$app->params['domains'];
        $newLang = $domains[$languageCode]['language'];

        Yii::info('  $domains = '. print_r($domains, true));

        $redirect = $newLang !== $oldLang;

        $url = Yii::$app->request->referrer;
        if ($redirect) {
            $oldDomain = parse_url($url, PHP_URL_HOST);
            $newDomain = $domains[$languageCode]['domain'];
            $url = str_replace($oldDomain, $newDomain, $url);
        }

        Yii::info('  $oldLang = '. $oldLang);
        Yii::info('  $newLang = '. $newLang);
        Yii::info('  $redirect = '. $redirect);
        Yii::info('  $url = '. $url);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'redirect' => $redirect,
            'url' => $url,
            'code' => 200,
        ];
    }
}
